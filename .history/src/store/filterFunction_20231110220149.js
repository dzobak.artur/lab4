// utils.js
export function checkDirectory(direct, filter) {
    for (const key in filter) {
      if (filter[key] && filter[key] !== direct[key]) return false;
    }
    return true;
  }
  