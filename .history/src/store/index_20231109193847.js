import { createStore } from "vuex";


//Довідник студента. База даних предметів: назва предмету, кількість годин, викладач,
//рейтинг. Організувати вибір за довільним запитом.
export default createStore({
  state: {
    studentDirectory : [
      {
        id: 1,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 2,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 3,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 4,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 5,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 6,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      filtered
    ],
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
