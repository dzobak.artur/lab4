import { createStore } from "vuex";


//Довідник студента. База даних предметів: назва предмету, кількість годин, викладач,
//рейтинг. Організувати вибір за довільним запитом.
export default createStore({
  state: {
    studentDirectory : [
      {
        id: 1,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Ivanov'
      },
    ],
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {},
});
