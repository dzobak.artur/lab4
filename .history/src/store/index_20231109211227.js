import { createStore } from "vuex";

function checkdirectory(direct,filter){
  for (const key in filter) {
    if (filter[key] && filter[key] !== direct[key]) return false
  }
  return true
}
//Довідник студента. База даних предметів: назва предмету, кількість годин, викладач,
//рейтинг. Організувати вибір за довільним запитом.
export default createStore({
  state: {
    studentDirectory : [
      {
        id: 1,
        item: 'Фізика',
        hoursquantity : 4,
        teacher : 'Petrenko I.A',
        rating : 1,
      },
      {
        id: 2,
        item: 'Математика',
        hoursquantity : 2,
        teacher : 'Petrenko I.A',
        rating : 5,
      },
      {
        id: 3,
        item: 'Фізика',
        hoursquantity : 3,
        teacher : 'Petrenko I.A',
        rating : 5,
      },
      {
        id: 4,
        item: 'Фізика',
        hoursquantity : 5,
        teacher : 'Petrenko I.A',
        rating : 5,
      },
      {
        id: 5,
        item: 'Фізика',
        hoursquantity : 5,
        teacher : 'Petrenko I.A',
        rating : 3,
      },
      {
        id: 6,
        item: 'Фізика',
        hoursquantity : 1,
        teacher : 'Petrenko I.A',
        rating : 4,
      },
    ],
    filteredItem: {},
  },
  getters: {
    getDirectory:({studentDirectory,filteredItem}) => studentDirectory.filter((direct) => checkdirectory(direct,filteredItem)),
    getDirectoryID:(state) => {
      return (directId) => state.studentDirectory.find((direct) => direct.id == directId)}
  },
  mutations: {
    removeDirectory(state, directId){
      state.studentDirectory = state.studentDirectory.filter((direct) => direct.id !== directId)
    },
    addDirectory(state, directory){
      state.studentDirectory.push(directory)
    },
    updateDirectory(state, directory){
      const directoryIndex = state.studentDirectory.filter((direct) => direct.id == directory.id)
      state.studentDirectory[directoryIndex] = directory 
    }
  },
  actions: {
    deleteDirectory({ commit },idToRemove){
      commit('removeDirectory',idToRemove)
    },
    addDirectory({commit}, directObj){
      commit ('addDirectory',{
        id: new Date().getTime(),
        ...directObj,
      })
    },
    updateDirectory({commit})

  },
  modules: {},
});
