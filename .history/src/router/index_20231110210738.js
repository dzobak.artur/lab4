import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import ContactPage from "../views/ContactPage.vue";
import ChangeDirectory from "../views/ChangeDirectory"

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: '/info',
    name: 'contacts',
    component: ContactPage,
},
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
